use std::path::Path;
use openssl::symm::{decrypt, encrypt, Cipher};
use anyhow::Result;

const KEY: &[u8; 16] = b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F";
const IV: &[u8; 16] = b"\x00\x01\x02\x03\x04\x05\x06\x07\x00\x01\x02\x03\x04\x05\x06\x07";

pub fn crawl_files(
    path: &Path,
    filter: Option<&dyn Fn(&Path) -> bool>,
    callback: &dyn Fn(&Path) -> Result<()>,
) -> Result<()> {
    for entry in path.read_dir()? {
        let entry = entry?;
        if entry.file_type()?.is_dir() {
            crawl_files(&entry.path(), filter, callback)?;
        } else {
            if let Some(filter) = filter {
                if filter(&entry.path()) {
                    callback(&entry.path())?;
                }
            } else {
                callback(&entry.path())?;
            }
        }
    }
    Ok(())
}

pub fn aes_128_encrypt(plain: &[u8]) -> Result<Vec<u8>> {
    encrypt(Cipher::aes_128_cbc(), KEY, Some(IV), plain).map_err(|e| e.into())
}

pub fn aes_128_decrypt(plain: &[u8]) -> Result<Vec<u8>> {
    decrypt(Cipher::aes_128_cbc(), KEY, Some(IV), plain).map_err(|e| e.into())
}
