# rustomware

Proof of concept ransomware that encrypts files with AES-128 from a base directory, then decrypts (I hope) them immediately after.

## Build

> cargo build [--release]

## Usage

> rustomware [base directory]
